FROM debian:jessie

RUN apt-get update
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y nodejs

ENV APP_NAME docker-fun
ENV PORT 3000

RUN mkdir -p /$APP_NAME
WORKDIR /$APP_NAME

COPY . .

RUN npm install

EXPOSE 3000

CMD trap exit TERM; npm start & wait
